#!/usr/bin/env python
# coding: utf-8

# In[148]:


# Importing all the required Python libraries and LDA (library file).
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn
from pandas import DataFrame
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn import tree
import sklearn.model_selection as sms
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz
import pydotplus
from sklearn.tree.export import export_text
import warnings
warnings.filterwarnings('ignore')


# In[149]:


# Loading the data.
data = pd.read_csv(r"G:\BGSU_MSAS\As_a_Student\Spring_2020\CS_7200\Assignment_3\cs7200_sp2020_a03_singh\Concrete_Data.csv")


# In[150]:


# Separating predictors and target variables.
X = data.iloc[:,[0,1,2,3,4,5,6,7]]
Y = data.iloc[:,[8]]


# # Decision Tree Regression

# # Question 1

# In[151]:


# Fitting and evaluating regression tree with max_features = 'auto'.
regr_auto = DecisionTreeRegressor(max_features="auto", random_state = 0)   
regr_auto.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_auto, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# In[152]:


# Fitting and evaluating regression tree with max_features = 'sqrt'.
regr_sqrt = DecisionTreeRegressor(max_features="sqrt", random_state = 0)   
regr_sqrt.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_sqrt, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# In[153]:


# Fitting and evaluating regression tree with max_features = 'log2'.
regr_log2 = DecisionTreeRegressor(max_features="log2", random_state = 0)   
regr_log2.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_log2, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# # Question 2

# # Case1 : max_features = 'auto'

# In[154]:


# Finding 'best' ccp_alpha value when max_features = 'auto'. Also, evaluating the tree depth for different ccp_alpha.
rmse = []
depth = []
for i in np.linspace(0, 1, num=21):
    regr = DecisionTreeRegressor(max_features="auto", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))
    depth.append(regr.get_depth())


# In[155]:


fig, axs = plt.subplots(2, sharex=True, sharey=False, gridspec_kw={'hspace': 0})
axs[0].plot(np.linspace(0, 1, num=21),rmse)
axs[1].plot(np.linspace(0, 1, num=21),depth)
fig.text(0.5, 0.04, 'ccp_alpha', ha='center', va='center')
axs[0].set_ylabel('Mean 5-Fold RMSE value')
axs[1].set_ylabel('Tree depth')
fig.suptitle('max_features = auto')


# # Case2 : max_features = 'sqrt'

# In[156]:


# Finding 'best' ccp_alpha value when max_features = 'sqrt'. Also, evaluating the tree depth for different ccp_alpha.
rmse = []
depth = []
for i in np.linspace(0, 1, num=21):
    regr = DecisionTreeRegressor(max_features="sqrt", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))
    depth.append(regr.get_depth())


# In[157]:


fig, axs = plt.subplots(2, sharex=True, sharey=False, gridspec_kw={'hspace': 0})
axs[0].plot(np.linspace(0, 1, num=21),rmse)
axs[1].plot(np.linspace(0, 1, num=21),depth)
fig.text(0.5, 0.04, 'ccp_alpha', ha='center', va='center')
axs[0].set_ylabel('Mean 5-Fold RMSE value')
axs[1].set_ylabel('Tree depth')
fig.suptitle('max_features = sqrt')


# # Case3 : max_features = 'log2'

# In[158]:


# Finding 'best' ccp_alpha value when max_features = 'log2'. Also, evaluating the tree depth for different ccp_alpha.
rmse = []
depth = []
for i in np.linspace(0, 1, num=21):
    regr = DecisionTreeRegressor(max_features="log2", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))
    depth.append(regr.get_depth())


# In[159]:


fig, axs = plt.subplots(2, sharex=True, sharey=False, gridspec_kw={'hspace': 0})
axs[0].plot(np.linspace(0, 1, num=21),rmse)
axs[1].plot(np.linspace(0, 1, num=21),depth)
fig.text(0.5, 0.04, 'ccp_alpha', ha='center', va='center')
axs[0].set_ylabel('Mean 5-Fold RMSE value')
axs[1].set_ylabel('Tree depth')
fig.suptitle('max_features = log2')


# # Question 3 (Best Tree found according to the above plots)

# In[160]:


# Fitting the best tree.
best_regr = DecisionTreeRegressor(max_features="auto", random_state = 0, ccp_alpha=0)
best_regr.fit(X, Y)


# In[161]:


# Graphically visualizing the best tree.
dot_data = StringIO()
export_graphviz(best_regr, out_file=dot_data, filled=True, rounded=True, special_characters=True)
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/graphviz-2.38/release/bin'
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
Image(graph.create_png())


# In[162]:


# Printing feature importances for the best tree chosen.
best_regr.feature_importances_


# In[163]:


# Printing root node in the best tree.
export_text(best_regr, feature_names=list(X))[5:20]


# # Random Forest Regression

# # Question 1

# # Finding the best method for choosing the number of maximum features

# In[164]:


# Fitting and evaluating random forest regressor with max_features = 'auto'.
regr_auto = RandomForestRegressor(max_features="auto",random_state=0)   
regr_auto.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_auto, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# In[165]:


# Fitting and evaluating random forest regressor with max_features = 'sqrt'.
regr_sqrt = RandomForestRegressor(max_features="sqrt",random_state=0)   
regr_sqrt.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_sqrt, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# In[166]:


# Fitting and evaluating random forest regressor with max_features = 'log2'.
regr_log2 = RandomForestRegressor(max_features="log2",random_state=0)   
regr_log2.fit(X, Y)
cv_rmse_scores_rf = sms.cross_val_score(regr_log2, X, Y, cv=5,scoring='neg_mean_squared_error')
print(np.sqrt(abs(cv_rmse_scores_rf)))
print("Mean 5-Fold RMSE: {}".format(np.mean(np.sqrt(abs(cv_rmse_scores_rf)))))


# # Finding best value for ccp_alpha

# # Case1 : max_features = 'auto'

# In[167]:


# Finding 'best' ccp_alpha value when max_features = 'auto'.
rmse = []
for i in np.linspace(0, 1, num=21):
    regr = RandomForestRegressor(max_features="auto", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))


# In[168]:


plt.plot(np.linspace(0, 1, num=21),rmse)
plt.ylabel('Mean 5-Fold RMSE value')
plt.xlabel('ccp_alpha')
plt.title('max_features = auto')


# # Case2 : max_features = 'sqrt'

# In[169]:


# Finding 'best' ccp_alpha value when max_features = 'sqrt'.
rmse = []
for i in np.linspace(0, 1, num=21):
    regr = RandomForestRegressor(max_features="sqrt", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))


# In[170]:


plt.plot(np.linspace(0, 1, num=21),rmse)
plt.ylabel('Mean 5-Fold RMSE value')
plt.xlabel('ccp_alpha')
plt.title('max_features = sqrt')


# # Case3 : max_features = 'log2'

# In[171]:


# Finding 'best' ccp_alpha value when max_features = 'log2'.
rmse = []
for i in np.linspace(0, 1, num=21):
    regr = RandomForestRegressor(max_features="log2", random_state = 0, ccp_alpha=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))


# In[172]:


plt.plot(np.linspace(0, 1, num=21),rmse)
plt.ylabel('Mean 5-Fold RMSE value')
plt.xlabel('ccp_alpha')
plt.title('max_features = log2')


# # Fitting the best model according to above plots

# In[173]:


# Fitting the best model.
best_regr = RandomForestRegressor(max_features="log2", random_state = 0, ccp_alpha=0)
best_regr.fit(X, Y)


# In[174]:


# Printing feature importances for the best model chosen.
best_regr.feature_importances_


# # Question 2

# In[175]:


# Finding the optimal value for n_estimators.
rmse = []
for i in range(1, 100):
    regr = RandomForestRegressor(max_features="log2", random_state = 0, ccp_alpha=0, n_estimators=i)
    regr.fit(X, Y)
    cv_rmse_scores_rf = sms.cross_val_score(regr, X, Y, cv=5,scoring='neg_mean_squared_error')
    rmse.append(np.mean(np.sqrt(abs(cv_rmse_scores_rf))))


# In[176]:


plt.plot(range(1, 100),rmse)
plt.ylabel('Mean 5-Fold RMSE value')
plt.xlabel('n_estimators')


# # Question 3 (Final optimal model with n_estimator = 17)

# In[177]:


# Fitting the final optimal model.
optimal_regr = RandomForestRegressor(max_features="log2", random_state = 0, ccp_alpha=0, n_estimators=17)
optimal_regr.fit(X, Y)


# In[178]:


# Printing feature importances for the final optimal model chosen.
optimal_regr.feature_importances_


# In[ ]:




