# Decision Tree Regression

1.) The best method for choosing the number of max features is found out to be 'auto', because of its least RMSE among other methods. The table below shows the results.

|Method for max_features|Mean 5-fold RMSE value (approx.)|
| --- | --- |
|auto| 11.022|
|sqrt|12.833|
|log2|15.134|

2.) The "best" ccp_alpha values (along with corresponding tree depth) across different methods for max_features are given in a table below,

|Method for max_features|"Best" ccp_alpha value (approx.)|Corresponding tree depth
| --- | --- | --- |
|auto| 0|20
|sqrt|0.85|9
|log2|0.3|12

Overall, the best tree chosen is the one with max_features = 'auto' and ccp_alpha = 0 because of its overall lower RMSE value. However, this tree has large tree depth (not pruned, ccp_alpha = 0). Thus it is likely to overfit.

3.) The best tree is printed graphically in my code. The feature importance is given in the table below,

|Features|Importances (approx.)|
| --- | --- |
|X_1|0.36|
|X_2|0.11|
|X_3|0.01|
|X_4|0.11|
|X_5|0.03|
|X_6|0.03|
|X_7|0.03|
|X_8|0.30|

Most important variables: X_1 and X_8.

Root node: Age <= 21.

**Random Forest Regression**

1.) The best method for choosing the number of max features is found out to be 'log2', because of its least RMSE among other methods. The table below shows the results.

Note: The concept of tree depth doesn't apply to random forest because a model is a collection of more than one tree.

|Method for max_features|Mean 5-fold RMSE value (approx.)|
| --- | --- |
|auto| 10.16|
|sqrt|9.82|
|log2|9.54|

The "best" ccp_alpha values across different methods for max_features are given in a table below,

|Method for max_features|"Best" ccp_alpha value (approx.)|
| --- | --- |
|auto| 0|
|sqrt|0|
|log2|0|

On considering just the max_features and ccp_alpha as the only constraints, the best model chosen is the one with max_features = log2 and ccp_alpha = 0.

The feature importance is given in the table below,

|Features|Importances (approx.)|
| --- | --- |
|X_1|0.24|
|X_2|0.06|
|X_3|0.04|
|X_4|0.12|
|X_5|0.08|
|X_6|0.05|
|X_7|0.06|
|X_8|0.34|

Most important variables: X_1 and X_8.

Moderately important variable: X_4.

2.) The optimal number of estimators were found to be 17 (or more). It achieved the lowest RMSE at n_estimators = 17, and then the RMSE almost remained constant as I increased the n_estimator further.

Overall, the *final optimal model* chosen is the one with max_features = log2, ccp_alpha = 0 and n_estimators = 17.


3.) The feature importance is given in the table below,

|Features|Importances (approx.)|
| --- | --- |
|X_1|0.25|
|X_2|0.05|
|X_3|0.04|
|X_4|0.11|
|X_5|0.08|
|X_6|0.05|
|X_7|0.07|
|X_8|0.34|


Most important variables: X_1 and X_8.
Moderately important variable: X_4.

**Final Pick**

According to the analysis done, the random forest regressor (with max_features = log2, ccp_alpha = 0, and n_estimators =17) achieved the least RMSE among all the other combinations of models considered.

